import static org.junit.Assert.*;

public class Test {

    @org.junit.Test
    public void eq() {
        Fib num = new Fib();
        int result = num.current_num(8);
        int answer = 13;
        asserEquals(answer, result);
    }
}