public class Fib {
    public int current_num(int num) {
        int k = 0; int l = 1;
        for (int i = num; i >= 0; i--){
            int temp = l;
            l = temp + k;
            k = temp;
        }
        return l;
    }
}